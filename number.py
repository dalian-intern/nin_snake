import random
import turtle

secret = random.randint(1,100)
while True:
	def drawSnake(rad,angle,len,neckrad):
		turtle.speed(2)
		for i in range(len):
			turtle.circle(rad,angle)
			turtle.circle(-rad,angle)
		turtle.circle(rad,angle/2)
		turtle.fd(rad)
		turtle.circle(neckrad+1, 180)
		turtle.fd(rad*2/3)

	def main():
			turtle.setup(500,400,0,0)
			pythonsize=30
			turtle.pensize(pythonsize)
			turtle.seth(40)
			drawSnake(40,80,2,pythonsize/2)
			input()
	temp = input("番号を入力してください( 1から  100まで):\n")
	guess = int(temp)
	if guess == secret:
	     print("あなたはそれを得た、それは素晴らしいです！")
	     main()
	     break
	else :
	     if guess<secret:
	        print("それは正しい数よりも小さいです。")
	     else :
	        print("正しい数より多い")


print("ゲームは終わった。")